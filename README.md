# Plesk Default Web

**Plesk Default Web** - Einfaches und optimiertes benutzerdefiniertes Template für virtuelle Hosts für Webserver mit Plesk Obsidian. Das Template enthält auch angepasste Fehlerseiten.


## Vorschau

[![Coming Soon Vorschau](img/preview.png)](https://dwolke.de)

**[Live ansehen](https://my-plesk.dw-labs.de)**


## Download und Installation

1. Lade die neueste Version des Templates herunter: [Releases](https://github.com/dw-labs/plesk-default-web/releases)
2. Melde Dich an Deinem Plesk-Server an. 
3. Um das benutzerdefinierte Template hochzuladen, gehe zu **Tools & Einstellungen** > **Template für virtuelle Hosts** (in der Gruppe **Tools & Ressourcen**) und klicke auf **Datei auswählen**. Suche dann die heruntergeladene Zip-Datei und klicke auf **Datei senden**.




## Fehler, Probleme, Anregungen

Kannst Du direkt [hier](https://github.com/dw-labs/plesk-default-web/issues) eintragen.



## Copyright and License

Copyright 2012-2020 by dwLabs. Code released under the [MIT](https://github.com/dw-labs/plesk-default-web/blob/master/LICENSE) license.

